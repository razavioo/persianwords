//
//  Game.swift
//  PersianWords
//
//  Created by Seyyed Emad Razavi on 1/30/19.
//  Copyright © 2019 Seyyed Emad Razavi. All rights reserved.
//

import Foundation

class Game {
    required init (emad: String){
        
    }
    
    func getMockData() -> [Test] {
        var testList : [Test] = []
        
        for i in 1...5 {
            var question : [String] = []
            for j in 1...3 {
                question[j] = "Test " + String(i) + " : Answer " + String(j)
            }
            
            testList[i] = Test(questions: question, correctIndex: 0)
        }
        
        return testList
    }
    
}

class Test {
    var questions: [String] = []
    var correctIndex : Int
    
    required init(questions : [String], correctIndex : Int){
        self.questions = questions
        self.correctIndex = correctIndex
    }
    
}


